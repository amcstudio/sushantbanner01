'use strict';
~ function() {
    var 
    	bgExit = document.getElementById("bgExit"),
        bgDrop = document.getElementById("bgDrop"),
        biebersDrop = document.getElementById("biebersDrop"),
        ad = document.getElementById("mainContent");

    window.init = function() {
		bgExit.addEventListener("click", clickHandler,false)
		
		createDrops(bgDrop,-100,-100,"drop");
		createDrops(biebersDrop,150,50,"drop1");
		createSparkles('bgSparks');
        
        var tl = new TimelineMax({});
        tl.set(ad, { perspective: 1000, force3D: true })
        
		.to(["#bieber,#bieberBg"],7,{ scale:1.45,x:20,y:-37, ease: Sine.easeInOut },"+=1.5")
		.to(["#bieberBlur,#bieberBgBlur"],5,{opacity:1, scale:1,x:-3,y:-13, ease: Sine.easeInOut },"-=5.5")


		.to('#copyOne', 0.8, {opacity: 1, repeat: 1, repeatDelay: 1.2, yoyo: true, ease: Sine.easeInOut},"-=5")
		.to('#logo', 0.8, {opacity: 1, repeat: 1, repeatDelay: 1.2, yoyo: true, ease: Sine.easeInOut},"-=2")
		.to('#doodle', 0.8, {opacity:1, ease: Sine.easeInOut})
        .to('#cta', 0.8, {scale: 1, opacity: 1, ease: Elastic.easeOut.config(1, 0.3)})
        
		

		function createDrops(dropPos, dropLeftPos,dropTopPos,dropName){
			for(var i=0; i<5; i++){
			var	drops=document.createElement("div");
				drops.className=dropName;
				dropPos.appendChild(drops);
				drops.style.opacity=0;
				drops.style.top = (150 + Math.random()*dropTopPos) + "px";
				drops.style.left = (150 + Math.random()*dropLeftPos) + "px";
		

			var randomOpacity=Math.random();
			var speed=Math.round(Math.random()*5);
			var apper=Math.round(Math.random()*5);

			TweenMax.to( drops,1, {opacity:randomOpacity, ease: Sine.easeInOut});
			TweenMax.to( drops, speed, {delay:apper,y:250, ease: Sine.easeInOut});		
			}
		}

     function createSparkles() {

        var sparks = document.getElementById("bgSparks")

        for (var i = 0; i < 45; i++) {
           var xPos = -180 + Math.random() * 300;
           var yPos = -145 + Math.random() * 250;
           var spark = document.createElement('div');
           spark.className = 'sparkle';

            sparks.appendChild(spark);
            TweenMax.set(spark,{rotation:Math.random()*200})
			TweenMax.to(spark, 1 + Math.random(), { delay: Math.random() * .5, scale: Math.random() * 1.8, y: yPos, x: xPos, opacity:0.5 +( Math.random()*1), ease: Back.easeOut.config(2), });
            TweenMax.to(spark, 1 + Math.random() * 2, { delay: 2 + Math.random() * 2, opacity: 0, yoyo: true,repeat: -1,ease: Sine.easeInOut });

        }
    }

    function clickHandler(e){
        e.preventDefault();
        window.open(window.clickTag);
    }
}
}();

window.onload = init();